# Three.JS Minesweeper

Minesweeper game made with Three.JS. The game will be played from a 1st person prespective, where the floor is the minesweeper board and the player has to dig the squares that don't have mines. When a square is cleared, it will then show the number of squares with mines adjacent to it. The player can also mark the squares that he thinks have mines. If a square with a mine is digged, it will blow and the player loses.

## Difficulty levels
- Beginner (8x8 grid with 10 mines)
- Intermediate (16x16 grid with 40 mines)
- Expert (24x24 grid with 99 mines)
- Custom

## Notes
- Some blocks of code are based on [this tutorial](https://docs.microsoft.com/en-us/windows/uwp/get-started/get-started-tutorial-game-js3d).
- Even though this project was built on a UWP application, the use of a browser to run the game is advisable, as the UWP app suffers from ["movement lag and unregistered keyUp events"](https://docs.microsoft.com/en-us/windows/uwp/get-started/get-started-tutorial-game-js3d#5-adding-player-movement).