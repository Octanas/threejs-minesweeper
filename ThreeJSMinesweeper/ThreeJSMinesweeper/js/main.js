﻿/////// HTML ELEMENTS
var messagePause = document.getElementById("messagePause"); // Pause menu
var pause = false;

var messageGameOver = document.getElementById("gameOver");  // Game over text
var gameOver = false;

var messageVictory = document.getElementById("victory");    // Victory text
var victory = false;

var menu = document.getElementById("menu"); // Selection menu
menu.style.display = '';    // Shows menu
var menuON = true;

var menuCustom = document.getElementById("menuCustom"); // Custom menu

var buttons = document.getElementById("buttons");   // Buttons from the selection menu

var container = document.getElementById("container");   // Container where the scenario will be rendered

var stats = document.getElementById("stats");
stats.style.display = "none";

// Container for the time
var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = 0;   // Seconds elapsed

var mineNumber = document.getElementById("minesLeft");  // Number of mines left to mark


////// PLAYER MOVEMENT
// Flags to determine which direction the player is moving (WASD)
var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;

var playerVelocity = new THREE.Vector3();   // Velocity vector for the player

var PLAYERSPEED = 800.0;    // How fast the player will move

var PLAYERCOLLISIONDISTANCE = 20;   // Distance from an object at which a colision will be triggered

var controls;
var controlsEnabled = false;    // Flag to determine if the controls should move the player

var started = false;    // Indicates if the player already started the game (the clock only starts after the player opens the first tile)

////// SCENE COMPONENTS
var UNITWIDTH = 20; // Width of a tile

var map_width, map_height, map_numMines;    // Map dimensions and characteristics

var collidableObjects = []; // Collidable objects (only the invisible walls around the map)

var mineSquares = [];   // Tiles that have a mine
var openableObjects = [];  // Tiles that can be clicked (which don't have a flag and haven't been opened yet)
var highlightableObjects = []; // Tiles that can be highlighted
                            // Every tile will be in the array during the entire game (even after being clicked, they are still highlighted)
var numbers = [];   // Numbers that will be added to the scene
var selected;   // Tile that is selected
var selectionGrid;   // Selection Grid

var cleanTilesOpened = 0;   // To keep track of the tiles opened (to trigger victory)

////// TECHNICAL STUFF
var clock;  // Clock to keep track of the change in time (delta) it takes to render new frames

var camera, scene, renderer;

var textFont = null;    // Text font used to create the text objects

var manager = new THREE.LoadingManager();   // Loading manager to load external components


//explode animation variables
var totalParts = 1000;
var dirs = [];
var parts = [];
var partsSpeed = 80;
var partsSize = 10;
var partsColors = [0xff5000, 0xff9400, 0xff0000, 0x996600, 0xffd454];

// When everything is finished loading, this function will execute
manager.onLoad = function () {

    menuSelection();
    init();
}

var fontLoader = new THREE.FontLoader(manager); // While this is used to load, the manager won't begin

// Loads the font that will be used
fontLoader.load(
    //'fonts/helvetiker_regular.typeface.json',
    'https://raw.githubusercontent.com/rollup/three-jsnext/master/examples/fonts/helvetiker_regular.typeface.json',
    function (font) {
    textFont = font;
});

function init()
{
    scene = new THREE.Scene();  // Creates the scene where everything will be inserted

    // Rendering settings
    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setClearColor(0xcccccc);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    container.appendChild(renderer.domElement).style.display = "block"; // Connects the renderer to the HTML container

    // Defines camera position
    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);

    controls = new THREE.PointerLockControls(camera);   // Adds the camera to the controls
    scene.add(controls.getObject());

    addLights();    // Adds lights to the scene

    window.addEventListener('resize', onWindowResize, false)    // Fixes the canvas when the window is resized

    container.addEventListener('mousedown', mouseClick, false);  // Event for any mouse click
    document.addEventListener('keydown', pauseGame, false);  // Event to detect the player pausing the game

    clock = new THREE.Clock();
    listenForPlayerMovement();  // Initializes events to detect the user pressing the WASD keys to move

    animate();
}

function animate() {
    render();
  
    requestAnimationFrame(animate); // Updates the render continuously

    var delta = clock.getDelta();   // This will be used to calculate the time passed between frames

    animatePlayer(delta);
    detectSelected();
}

function render() {
    var pCount = parts.length;
    while (pCount--) {
        parts[pCount].update();
    }

    renderer.render(scene, camera); // Renders the scene
}

function menuSelection() {

    // Button 8x8
    buttons.children[0].onclick = function () {
        menu.style.display = "none";
        menuON = false;

        map_height = 8; map_width = 8; map_numMines = 10;

        container.requestPointerLock(); // Locks the mouse controls to the canvas
        crosshair.style.display = '';
        createMap();
    }

    // Button 16x16
    buttons.children[1].onclick = function () {
        menu.style.display = "none";
        menuON = false;
        map_height = 16; map_width = 16; map_numMines = 40;

        container.requestPointerLock(); // Locks the mouse controls to the canvas
        crosshair.style.display = '';
        createMap();
    }

    // Button 24x24
    buttons.children[2].onclick = function () {
        menu.style.display = "none";
        menuON = false;
        map_height = 24; map_width = 24; map_numMines = 99;

        container.requestPointerLock(); // Locks the mouse controls to the canvas
        crosshair.style.display = '';
        createMap();
    }

    // Button Custom
    buttons.children[3].onclick = function () {
        menu.style.display = "none";
        menuCustom.style.display = '';

        // Button Create (Custom Menu)
        menuCustom.children[3].onclick = function () {

            var temp_width = document.getElementById("width").value;
            var temp_height = document.getElementById("height").value;
            var temp_numMines = document.getElementById("numMines").value;

            if (temp_width > 1 && temp_width <= 80 &&
                temp_height > 1 && temp_height <= 80 &&
                temp_numMines >= 1 && temp_numMines <= 999 &&
                temp_numMines < temp_width * temp_height)
            {
                menuCustom.style.display = "none";
                menuON = false;

                map_width = temp_width;
                map_height = temp_height;
                map_numMines = temp_numMines;

                container.requestPointerLock(); // Locks the mouse controls to the canvas
                crosshair.style.display = '';
                createMap();
            }
        }

        // Button Return (Custom Menu)
        menuCustom.children[4].onclick = function () {
            // Pressing the Return button goes back to main menu
            menuCustom.style.display = "none";
            menu.style.display = '';
        }   
    }

    document.addEventListener('pointerlockchange', lockChange, false);  // To enable and disable the controls
}

function lockChange() {
    if (document.pointerLockElement === container)
        controls.enabled = true;
    else
        controls.enabled = false;
}

function pauseGame() {

    if (event.keyCode == 80)    // 'P' key
    {
        // The game will be paused if it isn't already paused and the player isn't on a menu
        if (pause == false && menuON == false) {
            pause = true;

            messagePause.style.display = '';
            controls.enabled = false;
            document.exitPointerLock();
        }
        else {
            pause = false;

            messagePause.style.display = "none";

            // If the player isn't on a menu, the controls will be activated
            if (menuON == false) {
                controls.enabled = true;
                container.requestPointerLock();
            }

        }
    }
}

function listenForPlayerMovement() {

    // A key has been pressed
    var onKeyDown = function (event) {

        if (controls.enabled == true) {
            switch (event.keyCode) {

                case 38: // up
                case 87: // w
                    moveForward = true;
                    break;

                case 37: // left
                case 65: // a
                    moveLeft = true;
                    break;

                case 40: // down
                case 83: // s
                    moveBackward = true;
                    break;

                case 39: // right
                case 68: // d
                    moveRight = true;
                    break;
            }
        }
    };

    // A key has been released
    var onKeyUp = function (event) {

        switch (event.keyCode) {

            case 38: // up
            case 87: // w
                moveForward = false;
                break;

            case 37: // left
            case 65: // a
                moveLeft = false;
                break;

            case 40: // down
            case 83: // s
                moveBackward = false;
                break;

            case 39: // right
            case 68: // d
                moveRight = false;
                break;
        }
    };

    // Add event listeners for when movement keys are pressed and released
    document.addEventListener('keydown', onKeyDown, false);
    document.addEventListener('keyup', onKeyUp, false);
}

function animatePlayer(delta) {
    // Gradual slowdown
    playerVelocity.x -= playerVelocity.x * 10.0 * delta;
    playerVelocity.z -= playerVelocity.z * 10.0 * delta;

    // The movemente is only made is the player doesn't collide with an object
    if (detectPlayerCollision() == false) {
        if (moveForward) {
            playerVelocity.z -= PLAYERSPEED * delta;
        }
        if (moveBackward) {
            playerVelocity.z += PLAYERSPEED * delta;
        }
        if (moveLeft) {
            playerVelocity.x -= PLAYERSPEED * delta;
        }
        if (moveRight) {
            playerVelocity.x += PLAYERSPEED * delta;
        }

        controls.getObject().translateX(playerVelocity.x * delta);
        controls.getObject().translateZ(playerVelocity.z * delta);
    } else {
        // If there is a colision, the player is completely stopped
        playerVelocity.x = 0;
        playerVelocity.z = 0;
    }

    // Rotates the numbers so they're always facing the player
    animateNumbers();

    // This will only happen when the player has triggered a game over
    if (gameOver && mineSquares.length != 0)
    {
        // Every time the player is animated, one of the mines is opened
        // Until all of them have been opened
        var mineTile = mineSquares[0];

        setFlag(mineTile, false);   // Removes the flags over the mine that will be opened

        openTile(mineTile, true);

        mineSquares.splice(0, 1);
    }

    // This will only happen when the player has triggered a victory
    if (victory && mineSquares.length != 0) {
        // Every time the player is animated, one of the flags is placed
        // Until all of them have been placed
        var mineTile = mineSquares[0];

        setFlag(mineTile, true);   // Placed the flags over the mine

        mineSquares.splice(0, 1);
    }
}

function detectPlayerCollision() {
    // The rotation matrix to apply to our direction vector
    // Undefined by default to indicate ray should coming from front
    var rotationMatrix;
    // Get direction of camera
    var cameraDirection = controls.getDirection(new THREE.Vector3(0, 0, 0)).clone();

    // Check which direction we're moving (not looking)
    // Flip matrix to that direction so that we can reposition the ray
    if (moveBackward) {
        rotationMatrix = new THREE.Matrix4();
        rotationMatrix.makeRotationY(degreesToRadians(180));
    }
    else if (moveLeft) {
        rotationMatrix = new THREE.Matrix4();
        rotationMatrix.makeRotationY(degreesToRadians(90));
    }
    else if (moveRight) {
        rotationMatrix = new THREE.Matrix4();
        rotationMatrix.makeRotationY(degreesToRadians(270));
    }

    // Player is not moving forward, apply rotation matrix needed
    if (rotationMatrix !== undefined) {
        cameraDirection.applyMatrix4(rotationMatrix);
    }

    // Apply ray to player camera
    var rayCaster = new THREE.Raycaster(controls.getObject().position, cameraDirection);

    // If our ray hit a collidable object, return true
    if (rayIntersect(rayCaster, PLAYERCOLLISIONDISTANCE)) {
        return true;
    } else {
        return false;
    }
}

// Takes a ray and sees if it's colliding with anything from the list of collidable objects
// Returns true if certain distance away from object
function rayIntersect(ray, distance) {
    var intersects = ray.intersectObjects(collidableObjects);
    for (var i = 0; i < intersects.length; i++) {
        // Check if there's a collision
        if (intersects[i].distance < distance) {
            return true;
        }
    }
    return false;
}

function animateNumbers() {

    for (var i = 0; i < numbers.length; i++) {
        var lookTarget = new THREE.Vector3();
        lookTarget.copy(controls.getObject().position);
        lookTarget.y = numbers[i].position.y;
        numbers[i].lookAt(lookTarget);
        numbers[i].rotateX(degreesToRadians(-90));
    }
}

function detectSelected() {
    // Gets the intersected objects (by the camera vector) with Raycaster
    var cameraDirection = controls.getDirection(new THREE.Vector3(0, 0, 0)).clone();
    var raycasterSelect = new THREE.Raycaster(controls.getObject().position, cameraDirection);

    var intersectedObjects = raycasterSelect.intersectObjects(highlightableObjects);

    selected = intersectedObjects[0];

    if (selected != undefined && selected.distance <= UNITWIDTH * 4) {
        // Creates the new selection grid
        var gridHelper = new THREE.GridHelper(UNITWIDTH, 1, 0xffee00, 0xffee00);
        gridHelper.position.set(selected.object.position.x, 1.6, selected.object.position.z);
        scene.add(gridHelper);

        // Updates the selection grid
        scene.remove(selectionGrid);
        selectionGrid = gridHelper;
    }
    else {
        selected = undefined;
        scene.remove(selectionGrid);
    }
}

function createMap()
{
    var width = parseInt(map_width);
    var height = parseInt(map_height);
    var numMines = parseInt(map_numMines);

    for (var i = 0; i < numMines; i++) {
        var placed = false;

        // Get a random position until a valid one is selected
        while (!placed)
        {
            var posX = Math.floor(Math.random() * width);
            var posZ = Math.floor(Math.random() * height);

            // If the position generated already has a mine, can't insert the tile
            if (mineSquares.findIndex(obj => obj.name === "tile".concat(posX, "_", posZ)) == -1)
            {
                var mineGeo = new THREE.BoxGeometry(UNITWIDTH, 0.1, UNITWIDTH);
                //var mineMat = new THREE.MeshPhongMaterial({ color: 0xFF4500 });
                var mineMat = new THREE.MeshPhongMaterial({ color: 0x9ec651 });
                var mine = new THREE.Mesh(mineGeo, mineMat);

                // Positions the tile
                mine.position.x = posX * UNITWIDTH + UNITWIDTH / 2;
                mine.position.z = posZ * UNITWIDTH + UNITWIDTH / 2;
                mine.position.y = 1.3;

                // Changes the name of the tile to 'tileX_Z'
                mine.name = "tile".concat(posX, "_", posZ);

                scene.add(mine);

                // Adds the tile to all the necessary arrays
                mineSquares.push(mine);
                highlightableObjects.push(mine);
                openableObjects.push(mine);

                placed = true;

                // Creates the grid square for this tile
                var gridHelper = new THREE.GridHelper(UNITWIDTH, 1, 0x000000, 0x000000);

                // Positions the grid square
                gridHelper.position.set(posX * UNITWIDTH + UNITWIDTH / 2, 1.4, posZ * UNITWIDTH + UNITWIDTH / 2);

                scene.add(gridHelper);
            }
        }
    }

    
    for (var i = 0; i < width; i++)
    {
        for (var j = 0; j < height; j++)
        {
            // If this location already has a mine tile, a tile won't be placed (no need to check for normal tiles)
            if (mineSquares.findIndex(obj => obj.name === "tile".concat(i, "_", j)) == -1)
            {
                // The number of adjacent mines will be calculated (max. 8)

                var numAdjMines = 0;

                for(var k = i - 1; k <= i + 1; k++)
                {
                    for(var l = j - 1; l <= j + 1; l++)
                    {
                        if(k >= 0 && k < width && l >= 0 && l < height)
                        {
                            // If this position has a mine tile, add to the number of adjacent mines
                            if (mineSquares.findIndex(obj => obj.name === "tile".concat(k, "_", l)) != -1)
                            {
                                numAdjMines++;
                            }
                        }
                    }
                }

                // Create the text object with the number of adjacent mines according to this tile's number of adjacent mines
                if (numAdjMines != 0) {
                    var geometry = new THREE.TextGeometry(numAdjMines.toString(), {
                        font: textFont,
                        size: UNITWIDTH / 2,
                        height: 0.2,
                        curveSegments: 12
                    });

                    geometry.name = numAdjMines;    // Defines the name of the geometry with the number for easy reading

                    geometry.center();

                    if (numAdjMines == 1) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x0000ff, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 2) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x008100, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 3) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0xff1300, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 4) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x000083, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 5) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x810500, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 6) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x2a9494, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 7) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x000000, specular: 0xffffff }
                        );
                    }
                    else if (numAdjMines == 8) {
                        var material = new THREE.MeshPhongMaterial(
                            { color: 0x808080, specular: 0xffffff }
                        );
                    }

                    var text = new THREE.Mesh(geometry, material);

                    // Positions the text
                    text.position.x = i * UNITWIDTH + UNITWIDTH / 2;
                    text.position.z = j * UNITWIDTH + UNITWIDTH / 2;
                    text.position.y = 1.2;
                    text.rotateX(degreesToRadians(-90));

                    // The text name is changed to 'numberX_Z'
                    text.name = "number".concat(i, "_", j);

                    // The text object is added to the necessary array
                    numbers.push(text);

                    // The text object will only be added to the scene after it's tile is opened
                    // This saves resources
                }

                // The floor tile is created
                var cubegeo = new THREE.BoxGeometry(UNITWIDTH, 0.1, UNITWIDTH);
                var cubemat = new THREE.MeshPhongMaterial({ color: 0x9ec651 });
                var cube = new THREE.Mesh(cubegeo, cubemat);

                // The tile is positioned
                cube.position.x = i * UNITWIDTH + UNITWIDTH / 2;
                cube.position.z = j * UNITWIDTH + UNITWIDTH / 2;
                cube.position.y = 1.3;

                // The tile name is changed to 'tileX_Z'
                cube.name = "tile".concat(i, "_", j);

                scene.add(cube);

                // The tile is added to the all the necessary arrays
                highlightableObjects.push(cube);
                openableObjects.push(cube);

                // Creates the grid square for this tile
                var gridHelper = new THREE.GridHelper(UNITWIDTH, 1, 0x000000, 0x000000);

                // Positions the grid square
                gridHelper.position.set(i * UNITWIDTH + UNITWIDTH / 2, 1.4, j * UNITWIDTH + UNITWIDTH / 2);

                scene.add(gridHelper);
            }
        }
    }

    // Creates the ground plane
    var groundGeo = new THREE.PlaneGeometry(width * UNITWIDTH, height * UNITWIDTH);
    var groundMat = new THREE.MeshPhongMaterial({ color: 0xA0522D, side: THREE.DoubleSide });
    var ground = new THREE.Mesh(groundGeo, groundMat);

    // Positions the ground plane
    ground.position.set((width * UNITWIDTH/2), 1, height * UNITWIDTH/2);
    ground.rotation.x = degreesToRadians(90);

    scene.add(ground);

    // Creates the perimeter walls to block the player from exiting the map
    var perimGeoFB = new THREE.PlaneGeometry(map_width * UNITWIDTH, 20);
    var perimGeoLR = new THREE.PlaneGeometry(map_height * UNITWIDTH, 20);

    // The material will be transparent
    var perimMat = new THREE.MeshPhongMaterial({ color: 0x000000, side: THREE.DoubleSide, opacity: 0, transparent: true });

    // Every object will be placed at the same height
    // That height will be the same height as the yawObject from the controls (controls.getObject())
    // Because the object that will collide with the walls is the camera
    var perimWallHeight = controls.getObject().position.y;

    var perimWallLeft = new THREE.Mesh(perimGeoLR, perimMat);
    perimWallLeft.position.set(0, perimWallHeight, map_height / 2 * UNITWIDTH);
    perimWallLeft.rotation.y = degreesToRadians(90);
    scene.add(perimWallLeft);

    var perimWallRight = new THREE.Mesh(perimGeoLR, perimMat);
    perimWallRight.position.set(map_width * UNITWIDTH, perimWallHeight, map_height / 2 * UNITWIDTH);
    perimWallRight.rotation.y = degreesToRadians(90);
    scene.add(perimWallRight);

    var perimWallFront = new THREE.Mesh(perimGeoFB, perimMat);
    perimWallFront.position.set(map_width / 2 * UNITWIDTH, perimWallHeight, 0);
    scene.add(perimWallFront);

    var perimWallBack = new THREE.Mesh(perimGeoFB, perimMat);
    perimWallBack.position.set(map_width / 2 * UNITWIDTH, perimWallHeight, map_height * UNITWIDTH);
    scene.add(perimWallBack);

    // The walls are pushed into the collidable objects array so they're used to calculate collisions
    collidableObjects.push(perimWallLeft);  
    collidableObjects.push(perimWallRight);
    collidableObjects.push(perimWallFront);
    collidableObjects.push(perimWallBack);

    // The player is positioned in the center of the map
    controls.getObject().translateX(width * UNITWIDTH / 2);
    controls.getObject().translateZ(height * UNITWIDTH / 2);

    stats.style.display = '';
    setInterval(setTime, 1000);

    mineNumber.innerHTML = numMines;
}

function addLights() {
    // Positions a light and places it in the scene
    var light = new THREE.DirectionalLight(0xffffff);
    light.position.set(1, 1, 1);
    scene.add(light);
}

function mouseClick(event) {
    // A player can only click if the game isn't paused, and if game over or victory haven't been triggered

    if (controls.enabled == false && !pause && !menuON) {
        container.requestPointerLock();
        return;
    }

    if (!pause && !gameOver && !victory && !menuON)
    {

        if (selected != undefined && selected.distance <= UNITWIDTH * 4) {
            // Left click
            if (event.button == 0) {

                console.log("Left clicked");

                openTile(selected.object, true);

                selected = undefined;
            }
            // Right click
            else if (event.button == 2) {

                console.log("Right clicked");

                setFlag(selected.object);
            }
        }
    }
}

function openTile(object, canBeMine)
{
    if (openableObjects.indexOf(object) != -1) {
        // Removes the tile from the scene
        scene.remove(object);

        // Removes the tile from the openable objects array
        var ind = openableObjects.indexOf(object);
        if (ind != -1)
            openableObjects.splice(ind, 1);

        // Selected's position
        var selectedXCenter = object.position.x;
        var selectedZCenter = object.position.z;

        // If canBeMine is false, this isn't checked (to save time, because adjacent tiles to a 0 tile can't be mines)
        if (canBeMine && mineSquares.indexOf(object) != -1) {
            console.log("BOOM!");

            // Mine color(sphere and spikes)
            var mineMat = new THREE.MeshPhongMaterial({ color: 0x343a47 });

            // Mine sphere
            var mineBodyGeo = new THREE.SphereGeometry(UNITWIDTH / 4, 32, 32);
            var mineBody = new THREE.Mesh(mineBodyGeo, mineMat);

            // Mine spikes
            var mineSpikeGeo = new THREE.CylinderGeometry(0.01, UNITWIDTH / 16, UNITWIDTH / 3, 32, 32);
            var mineSpike1 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike2 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike3 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike4 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike5 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike6 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike7 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike8 = new THREE.Mesh(mineSpikeGeo, mineMat);
            var mineSpike9 = new THREE.Mesh(mineSpikeGeo, mineMat);

            // Set sphere and spikes positions
            var deg = degreesToRadians(35);

            // Sets the mine's position
            mineBody.position.set(selectedXCenter, -1, selectedZCenter);

            mineSpike1.position.set(selectedXCenter, 4, selectedZCenter);
            mineSpike1.rotation.set(0, 0, 0);
            mineSpike2.position.set(selectedXCenter, 3, selectedZCenter + 3);
            mineSpike2.rotation.set(45, 0, 0);
            mineSpike3.position.set(selectedXCenter, 3, selectedZCenter - 3);
            mineSpike3.rotation.set(-45, 0, 0);
            mineSpike4.position.set(selectedXCenter - 3, 3, selectedZCenter);
            mineSpike4.rotation.set(0, 0, 45);
            mineSpike5.position.set(selectedXCenter + 3, 3, selectedZCenter);
            mineSpike5.rotation.set(0, 0, -45);
            mineSpike6.position.set(selectedXCenter - 1, 2.5, selectedZCenter + 1);
            mineSpike6.rotation.set(deg, 0, deg);
            mineSpike7.position.set(selectedXCenter - 1, 2.5, selectedZCenter - 1);
            mineSpike7.rotation.set(-deg, 0, deg);
            mineSpike8.position.set(selectedXCenter + 1, 2.5, selectedZCenter + 1);
            mineSpike8.rotation.set(deg, 0, -deg);
            mineSpike9.position.set(selectedXCenter + 1, 2.5, selectedZCenter - 1);
            mineSpike9.rotation.set(-deg, 0, -deg);

            // Adds the mine to the scene
            scene.add(mineBody);
            scene.add(mineSpike1);
            scene.add(mineSpike2);
            scene.add(mineSpike3);
            scene.add(mineSpike4);
            scene.add(mineSpike5);
            scene.add(mineSpike6);
            scene.add(mineSpike7);
            scene.add(mineSpike8);
            scene.add(mineSpike9);

            // C_CHECK: Implement explosions
            // Explode everything

            // Game over
            gameOver = true;
            messageGameOver.style.display = ''; // Shows the game over text



            //explode animation
            parts.push(new ExplodeAnimation(selectedXCenter,selectedZCenter));
        }
        else {
            var i = -1;

            // Get the tile's coordenates so the associated number can be found
            // It is easier to get these values from the title than to calculate them based on the object's position
            var coordenates = object.name.substring(4);

            i = numbers.findIndex(obj => obj.name == "number".concat(coordenates));

            if (i != -1)
                // The number is added
                scene.add(numbers[i]);
            else {
                // If there isn't a number to add, that's because there are no adjacent mines
                // In that case, all the adjacent tiles should be opened
                // It is possible that this same operation could occur for the mines that will be opened here
                // So, this is a recursive function

                var parsedCoordenates = coordenates.split("_");

                var x = parseInt(parsedCoordenates[0]);
                var y = parseInt(parsedCoordenates[1]);

                for (var k = x - 1; k <= x + 1; k++) {
                    for (var l = y - 1; l <= y + 1; l++) {
                        // If a tile is found in this position, it will be opened (this same function is called)
                        var ind = openableObjects.findIndex(obj => obj.name === "tile".concat(k, "_", l));

                        if (ind != -1)
                            openTile(openableObjects[ind], false);  // canBeMine will be false, because no tile adjacent to a 0 tile can have a mine
                    }
                }
            }

            cleanTilesOpened++; // Increments the number of opened tiles

            // When all the clean tiles have been opened, trigger VICTORY
            if (cleanTilesOpened == map_width * map_height - map_numMines) {
                victory = true;
                messageVictory.style.display = '';  // Show victory message
            }
        }

        started = true; // The player has opened his first tile (this will be done everytime the player opens a tile, but it's no big deal)
    }
    else
    {
        // An opened tile has been clicked
        var coordenates = object.name.substring(4);
        var indNumber = numbers.findIndex(obj => obj.name == "number".concat(coordenates));

        // If there is a number in that place (that is, if the number of mines around that square isn't 0)
        if (indNumber != -1)
        {
            var numFlags = 0;

            var tilesToOpen = [];

            var parsedCoordenates = coordenates.split("_");
            var x = parseInt(parsedCoordenates[0]);
            var y = parseInt(parsedCoordenates[1]);

            // This will count the amount of flags around the opened tile and save the openable tiles
            for (var k = x - 1; k <= x + 1; k++)
            {
                for (var l = y - 1; l <= y + 1; l++)
                {
                    var flag = scene.getObjectByName("pole".concat(k, "_", l));
                    var indTile = openableObjects.findIndex(obj => obj.name === "tile".concat(k, "_", l));

                    if (flag != undefined)
                        numFlags++;
                    else if (indTile != -1)
                        tilesToOpen.push(openableObjects[indTile]);
                }
            }

            // If the number of mines around the opened tile is equal to the number of flags around it
            // All the openable tiles around it will be opened
            // NOTE: The tiles that will be openable can have mines if the player placed the flags in the wrong tiles
            if (numFlags == numbers[indNumber].geometry.name) {
                for (var i = 0; i < tilesToOpen.length; i++)
                    openTile(tilesToOpen[i], true);
            }
        }
    }
}

function setFlag(object, place)
{
    // the variable 'place' indicates if a flag should be placed or removed
    // if it's value is 'true', the flag will be placed (if it hasn't been placed yet)
    // if it's value is 'false', the flag will be removed (it there is one)
    // if it's value is undefined, the flag will be placed or removed according to it's state

    // Selected's position
    var selectedXCenter = object.position.x;
    var selectedZCenter = object.position.z;

    var coordenates = object.name.substring(4); // Part of the name of the flag
    var npole = "pole".concat(coordenates);
    var ntri = "tri".concat(coordenates);

    if ((place == undefined || place) && openableObjects.indexOf(object) != -1) {
        // Place flag
        console.log("Place flag");
        mineNumber.innerHTML = parseInt(mineNumber.innerHTML) - 1;
        
        // Removes the tile form the openable objects array
        var ind = openableObjects.indexOf(object);
        openableObjects.splice(ind, 1);

        // Creates the flag Pole
        var flagPoleMat = new THREE.MeshPhongMaterial({ color: 0xcccccc });
        var flagPoleGeo = new THREE.CylinderGeometry(0.3, 0.3, UNITWIDTH / 2, 32, 1);
        var flagPole = new THREE.Mesh(flagPoleGeo, flagPoleMat);
        scene.add(flagPole);

        // Creates the flag... cloth?
        var flagTriMat = new THREE.MeshBasicMaterial({ color: 0xff0000, side: THREE.DoubleSide });
        var flagTriGeo = new THREE.Geometry();
        flagTriGeo.vertices = [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 4, 0), new THREE.Vector3(4, 4, 4)];
        flagTriGeo.faces = [new THREE.Face3(0, 1, 2)];
        var flagTri = new THREE.Mesh(flagTriGeo, flagTriMat);
        scene.add(flagTri);

        // Positions the whole flag
        flagPole.position.set(selectedXCenter, UNITWIDTH / 3, selectedZCenter);
        flagTri.position.set(selectedXCenter, UNITWIDTH / 2.8, selectedZCenter);

        // Names the flag components
        flagPole.name = npole;
        flagTri.name = ntri;
    }
    else if ((place == undefined || !place) && scene.getObjectByName(npole) != undefined && scene.getObjectById(object.id) != undefined) {
        // Remove flag
        console.log("Remove flag");
        if (!gameOver) {
            mineNumber.innerHTML = parseInt(mineNumber.innerHTML) + 1;
        }

        // Removes the flag
        scene.remove(scene.getObjectByName(npole));
        scene.remove(scene.getObjectByName(ntri));

        // Adds the tile back to the openable objects array
        openableObjects.push(object);
    }
}

// Resizes the canvas to match the window
function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}

function radiansToDegrees(radians) {
    return radians * 180 / Math.PI;
}

function ExplodeAnimation(x, z) {
    var geometry = new THREE.Geometry();

    for (i = 0; i < totalParts; i++) {
        var vertex = new THREE.Vector3();
        vertex.x = x;
        vertex.z = z;
        vertex.y = 1;

        geometry.vertices.push(vertex);
        dirs.push({ x: (Math.random() * partsSpeed) - (partsSpeed / 2), y: (Math.random() * partsSpeed) - (partsSpeed / 2), z: (Math.random() * partsSpeed) - (partsSpeed / 2) });
    }
    var material = new THREE.ParticleBasicMaterial({ size: partsSize, color: partsColors[Math.round(Math.random() * partsColors.length)] });
    var particles = new THREE.ParticleSystem(geometry, material);

    this.object = particles;
    this.status = true;

    this.xDir = (Math.random() * partsSpeed) - (partsSpeed / 2);
    this.yDir = (Math.random() * partsSpeed) - (partsSpeed / 2);
    this.zDir = (Math.random() * partsSpeed) - (partsSpeed / 2);

    scene.add(this.object);

    this.update = function () {
        if (this.status == true) {
            var pCount = totalParts;
            while (pCount--) {
                var particle = this.object.geometry.vertices[pCount]
                particle.y += dirs[pCount].y;
                particle.x += dirs[pCount].x;
                particle.z += dirs[pCount].z;
            }
            this.object.geometry.verticesNeedUpdate = true;
        }
    }

}

// Increments the clock
function setTime() {
    // It will only increment if the game hasn't finished and isn't paused
    if (started && !pause && !gameOver && !victory) {
        ++totalSeconds;
        secondsLabel.innerHTML = pad(totalSeconds % 60);
        minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
    }
}

function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}